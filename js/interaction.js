// Various variables
var videoHalfWay = 0;
var formattedHalfWay = 0;

// Choice parts

var jsonSub = [{"start": 0.3, "end": 2.2, "text": "The dog and The bone"}, {
    "start": 4.317,
    "end": 10.517,
    "text": "Once a stray dog passing by a butcher<br />shop found a nice, juicy piece of bone."
}, {"start": 11.995, "end": 13.995, "text": "The dog was very pleased."}, {
    "start": 14.5,
    "end": 19.5,
    "text": "He clutched the bone hard with his teeth<br />and ran towards the woods to eat it."
}, {
    "start": 21.159,
    "end": 24.159,
    "text": "He then had to walk across<br />the <u>bridge</u> over a <a href=\"http://www.google.com\">stream</a>..."
}, {"start": 24.3, "end": 26.3, "text": "...through the gaps in the wooden bridge."}, {
    "start": 27.024,
    "end": 29.524,
    "text": "The dog could see his own <u>reflection</u>."
}, {
    "start": 31.2,
    "end": 35.2,
    "text": "But the <u>foolish</u> dog thought it was<br />another dog staring at him..."
}, {"start": 35.3, "end": 37.9, "text": "...with a nice juicy bone in his <u>mouth</u>."}, {
    "start": 38.902,
    "end": 43.902,
    "text": "He decided that he had to have<br />the other dog's bone as well."
}, {"start": 44.555, "end": 46.221, "text": "The greedy fellow barked."}, {
    "start": 46.24,
    "end": 49.74,
    "text": "As soon as he did this, the<br />bone fell into the water."
}, {"start": 51.315, "end": 55.048, "text": "The poor dog was terribly upset at losing his bone..."}, {
    "start": 55.283,
    "end": 59.016,
    "text": "...but he said to himself, \"Let that be a lesson to me.\""
}, {"start": 59.627, "end": 61.627, "text": "\"Never to be so greedy again.\""}]


var choicePart = 3;
var goodChoicePart = 7;
var badChoicePart = 20;
var goodChoiceChosen = false;

// Question variable
var question1Asked = false;

var video1;

$(document).ready(function () {
//load all subtitles in array


    $.featherlight.defaults.afterClose = playPauseVideo;

    video1 = $('#video1');

    $('.box1').on('click', function () {
        playPauseVideo('.persona1PopUp');
    });

    $('.box2').on('click', function () {
        playPauseVideo('.persona2PopUp');
    });

    $('.goodChoice').on('click', function () {
        goodChoiceChosen = true;
        $.featherlight.close();
        video1[0].currentTime = goodChoicePart;
    })

    $('.badChoice').on('click', function () {
        $.featherlight.close();
        video1[0].currentTime = badChoicePart;
    })

    $(video1).on('loadeddata', function () {
        videoHalfWay = Math.round(this.duration / 2);
    })

    $(video1).on('timeupdate', function () {
        var currentTime = Math.round(this.currentTime);
        var durationNum = Math.round(this.duration);
        // console.log('current time is :',currentTime);
        // console.log('find result  :',$(jsonSub.find(":time="+currentTime)));
        $.autotab({tabOnSelect: true});
        $('.alpha').autotab('filter', 'alpha');


        $.grep(jsonSub, function (ele, index) {
            endtime = Math.round(ele.end);
            console.log(endtime, ele.start);
            if ((Math.round(ele.start) == currentTime)) {

                $('.subbox').html(ele.text);
                var p = $('.subbox');
                var text = p.text().split(' ');
                for (var i = 0, len = text.length; i < len; i = i + 1) {
                    text[i] = '<a href="javascript:playPauseVideoText(\'' + text[i].replace(/[^A-Za-z0-9]/g, '') + '\')">' + text[i] + '</a>';
                }
                p.html(text.join(' '));

            }
            if (Math.round(ele.start) >= endtime) {
                $('.subbox').html('');
            }
        });

        //console.log('index in array : ',$.inArray(currentTime,jsonSub));

        var formattedCurrentTime = secondsToHms(currentTime);
        var formattedDurationTime = secondsToHms(durationNum)
        onTrackedVideoFram(formattedCurrentTime, formattedDurationTime)

        if (currentTime == choicePart && question1Asked == false) {
            question1Asked = true;
            video1[0].pause();
            $.featherlight($('.popUpQuestion1'))
        }

        if (currentTime == badChoicePart && goodChoiceChosen == true) {
            video1[0].pause();
        }

        if (currentTime == videoHalfWay) {
            // Halfway point
        }

        if (currentTime == durationNum) {
            // Video complete
        }


        // $(".custom-btn").focusin(function(){
        //   console.log(123);
        //     $(this).css("background-color", "#FFFFCC");
        // });


    });

});

function onTrackedVideoFram(curretTime, duration) {
    $('.current').text(curretTime);
    $('.duration').text(duration);
}

function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);
    return ((h > 0 ? h + ":" + (m < 10 ? "0" : "") : "") + m + ":" + (s < 10 ? "0" : "") + s);
}

function playPauseVideo(popUp) {
    if (video1[0].paused) {
        video1[0].play();
    } else {
        video1[0].pause();
        $.featherlight($(popUp));
    }
}

function playPauseVideoText(text) {
    var a = false;
    if (video1[0].paused) {
        video1[0].play();
    } else {
        video1[0].pause();
        PlaySound(text, 'Male');
        audiolinkMale = "<img src='images/speak.png' onclick=\"PlaySound(\'" + text + "\','Male')\" /> \n";
        //audiolinkFemale = "<button onclick=\"PlaySound(\'" + text + "\','Female')\"> UK English Female</button><br />\n";

        text= text.replace(/[^A-Za-z0-9]/g, '');

        wordHtml = "";resultP="<p id='result'></p>";
        for (i = 1; i <= text.length; i++){
            wordHtml += "<input  type=\"text\" id=\"alpha" + i + "\" class=\"alpha\" maxlength=\"1\" size=\"1\"   />\n";
        }
        wordHtml += "<input type='submit' class='hiddensubmit ' onfocus=\"checkword(\'" + text + "\');\" />";
        $('.popup').html('<div>'+audiolinkMale  + '</div> <div>' + resultP + '</div><div> ' + wordHtml+'</div>');
       // $('.popup').html(audiolinkMale + '  ' + audiolinkFemale + '  ' + resultP + ' ' + wordHtml);
        $.featherlight($('.popup'));

    }
}

function PlaySound(text, gender) {
    responsiveVoice.speak(text, "UK English " + gender);
}

function checkword(text, flag) {
    var i = 0, inputText = "", x;
    $('input[id^="alpha"]').each(function (input) {
        var value = $(this).val();
        var id = $(this).attr('id');
        // console.log(typeof  value,value.length);
        if (value.length != 0) {
            inputText+=value;
        }
    });



    $('p[id^="result"]').each(function (index,item) {

        var value = $(this).val();
        if(text.toLowerCase() == inputText.toLowerCase()){
            $(this).html('<span class="succ">Correct Answer</span>');
        }else{
            $(this).html('<span class="fail">Wrong Answer</span>');
        }


    });


}
